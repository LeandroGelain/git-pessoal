require "redis"
require "json"
class CarrinhoController < ApplicationController
  def index
    redis = Redis.new
    @all_products_keys = redis.keys('*')
    @list_products = []
    if @all_products_keys.count() > 0
      @all_products_values = redis.mget(@all_products_keys)
      (0..@all_products_values.count()-1).each do |index|
        product = @all_products_values[index]
        product = product.remove("\\")
        @list_products.push({@all_products_keys[index] =>  JSON.parse(product)})
      end
      @list_products
    end
  end


  def new
    redis = Redis.new
    list_keys = redis.keys('*').sort
    count_docs = list_keys.pop.to_i + 1
    redis.set(count_docs, [permited_params[:nome_produto], permited_params[:preco]])
    redirect_to root_path
  end
  def create
  end 

  def edit
  end

  def update 
    redis = Redis.new
    redis.set(permited_params[:id], [permited_params[:nome_produto], permited_params[:preco]])
    redirect_to root_path
  end


  def destroy
    redis = Redis.new
    redis.del(permited_params[:id])
    redirect_to root_path
  end

  private
  def permited_params
    params.permit(:nome_produto, :preco, :id)
  end
end


