import re

def map(frase):
    frase_split = frase.split()
    doc = []
    for palavra in frase_split:
        palavra = palavra.lower()
        palavra = re.sub(r'\W', "",  palavra)
        doc.append([palavra,1])
    return doc

def shuffle(maped_obj):
    keys = []
    for obj in maped_obj:
        key = obj[0]
        keys.append(key)
    sorted_keys = sorted(keys)
    
    sorted_obj = []
    for x in sorted_keys:
        sorted_obj.append([x, 1])
    return sorted_obj

def reduce_method(shuffled_obj):
    reducing = [shuffled_obj[0]]
    for obj in shuffled_obj:
        key = obj[0]
        value = obj[1]
        if key == reducing[-1][0]:
            reducing[-1][1] = reducing[-1][1] + value
        else:
            reducing.append([key, 1])
    return reducing

def index():
    with open("livro.txt", "r") as livro:
        linhas = "".join(livro.readlines())
    livro.close()
    # frase = "Sorte Sorte Sorte Sorte Sorte Sorte de quem te conheceu Sorte de quem teve' um pedaço do sorriso seu Sorte de quem teve por um momento seu coração Sorte de quem sentiu o toque da sua mão Azar de quem teve tudo isso por vários dias e perdeu de ter para sempre sua companhia"
    after_map = map(linhas)
    after_shuffle = shuffle(after_map)
    after_reduce = reduce_method(after_shuffle)
    
    

    return after_reduce
    
contagem = index()
print(contagem)